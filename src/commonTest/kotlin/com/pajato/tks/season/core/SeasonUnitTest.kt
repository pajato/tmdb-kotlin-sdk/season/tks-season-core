package com.pajato.tks.season.core

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.episode.core.Episode
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class SeasonUnitTest : ReportingTestProfiler() {
    @Test fun `When a default season object is serialized and deserialized, verify`() {
        val season = Season()
        val json = jsonFormat.encodeToString(season)
        assertEquals("{}", json)
        assertEquals(-1, jsonFormat.decodeFromString<Season>(json).id)
    }

    @Test fun `When a non-default season object is serialized and deserialized, verify`() {
        val idUnderscore = "xyzzy"
        val airDate = "today"
        val episodes = 6
        val list: List<Episode> = listOf()
        val name = "xyzzy"
        val overview = "A really good story."
        val id = 12
        val posterPath = "/xyzzy.jpg"
        val seasonNumber = 6
        val season = Season(idUnderscore, airDate, episodes, list, name, overview, id, posterPath, seasonNumber)
        val json = jsonFormat.encodeToString(season)
        val expected =
            """{"_id":"xyzzy","air_date":"today","episode_count":6,"name":"xyzzy",""" +
                """"overview":"A really good story.","id":12,"poster_path":"/xyzzy.jpg","season_number":6}"""
        assertEquals(expected, json)
        assertEquals(12, jsonFormat.decodeFromString<Season>(json).id)
    }
}
