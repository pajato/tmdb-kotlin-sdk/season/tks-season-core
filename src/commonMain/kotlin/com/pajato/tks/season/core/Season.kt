package com.pajato.tks.season.core

import com.pajato.tks.common.core.POSTER_PATH
import com.pajato.tks.episode.core.Episode
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Season(
    @SerialName("_id")
    val underscoreId: String = "",
    @SerialName("air_date")
    val airDate: String = "",
    @SerialName("episode_count")
    val episodeCount: Int = 0,
    val episodes: List<Episode> = listOf(),
    val name: String = "",
    val overview: String = "",
    val id: Int = -1,
    @SerialName(POSTER_PATH)
    val posterPath: String = "",
    @SerialName("season_number")
    val seasonNumber: Int = -1,
)
