package com.pajato.tks.season.core

import com.pajato.tks.common.core.SeasonKey

public interface SeasonRepo {
    public suspend fun getSeason(key: SeasonKey): Season
}
